using System;
using System.Collections.Generic;
using ImGuiNET;
using ImGuiNET.SampleProgram.XNA;

using Num = System.Numerics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Runtime.InteropServices;

namespace isometricparkfna.UI
{

    public struct DebugInfo
    {
        public float fps;
        public float[] pastFps;
        public TimeSpan drawTime;
        public Vector2 cameraPosition;
        public int treeCount;
        public Vector2 mouseGrid;
        public Boolean hasTree;
        public int tilesDrawn;
        public TimeSpan updateTime;
    }

    public class DebugWindow
    {

        private Texture2D _xnaTexture;
        private IntPtr _imGuiTexture;
        private bool show_test_window;

        public ImFontPtr monoFont;


        public DebugWindow(ImGuiRenderer _imGuiRenderer, GraphicsDevice graphicsDevice)
        {
            ImGuiIOPtr io = ImGui.GetIO();
            //io.Fonts.AddFontFromFileTTF(@"Content/iosevka-term-medium.ttf", 15);

#if DEBUG
            io.Fonts.AddFontFromFileTTF(@"Content/iosevka-medium.ttf", 15);
            io.Fonts.AddFontFromFileTTF(@"Content/iosevka-term-medium.ttf", 15);
            io.Fonts.AddFontFromFileTTF(@"Content/iosevka-term-medium.ttf", 17);
            io.Fonts.AddFontFromFileTTF(@"Content/iosevka-term-extendedmedium.ttf", 20);
            io.Fonts.AddFontFromFileTTF(@"Content/iosevka-term-extendedmedium.ttf", 25);
            io.Fonts.AddFontFromFileTTF(@"Content/iosevka-term-extendedmedium.ttf", 30);
#endif

            this.monoFont = io.Fonts.AddFontFromFileTTF(@"Content/iosevka-term-extendedmedium.ttf", 15);
            unsafe //god this sucks
            {
                ImFontConfigPtr config = ImGuiNative.ImFontConfig_ImFontConfig();
                config.MergeMode = true;
                var builder = new ImFontGlyphRangesBuilderPtr(ImGuiNative.ImFontGlyphRangesBuilder_ImFontGlyphRangesBuilder());
                // builder.AddText("\ue125\ue126");
                var icon_ranges = new ushort[] { 0xe000, 0xe1f4, 0 };
                GCHandle rangeHandle = GCHandle.Alloc(icon_ranges, GCHandleType.Pinned);
                io.Fonts.AddFontFromFileTTF(@"Content/typicons.ttf", 15, config, rangeHandle.AddrOfPinnedObject());


            }

            _imGuiRenderer.RebuildFontAtlas(); // Required so fonts are available for rendering


            _xnaTexture = CreateTexture(graphicsDevice, 300, 150, pixel =>
            {
                var red = (pixel % 300) / 2;
                return new Color(red, 1, 1);
            });

            // Then, bind it to an ImGui-friendly pointer, that we can use during regular ImGui.** calls (see below)
            _imGuiTexture = _imGuiRenderer.BindTexture(_xnaTexture);
        }

        public static Texture2D CreateTexture(GraphicsDevice device, int width, int height, Func<int, Color> paint)
        {
            //initialize a texture
            var texture = new Texture2D(device, width, height);

            //the array holds the color for each pixel in the texture
            Color[] data = new Color[width * height];
            for (var pixel = 0; pixel < data.Length; pixel++)
            {
                //the function applies the color according to the specified pixel
                data[pixel] = paint(pixel);
            }

            //set the color
            texture.SetData(data);

            return texture;
        }

        public virtual void ImGuiLayout()
        {

            float f = 0.0f;

            bool show_test_window = false;
            bool show_another_window = false;
            Num.Vector3 clear_color = new Num.Vector3(114f / 255f, 144f / 255f, 154f / 255f);
            byte[] _textBuffer = new byte[100];

            // 1. Show a simple window
            // Tip: if we don't call ImGui.Begin()/ImGui.End() the widgets appears in a window automatically called "Debug"
            {


                ImGui.Text("Hello, world!");
                ImGui.SliderFloat("float", ref f, 0.0f, 1.0f, string.Empty);
                ImGui.ColorEdit3("clear color", ref clear_color);
                if (ImGui.Button("Test Window")) show_test_window = !show_test_window;
                if (ImGui.Button("Another Window")) show_another_window = !show_another_window;
                ImGui.Text(string.Format("Application average {0:F3} ms/frame ({1:F1} FPS)", 1000f / ImGui.GetIO().Framerate, ImGui.GetIO().Framerate));

                ImGui.InputText("Text input", _textBuffer, 100);

                ImGui.Text("Texture sample");
                ImGui.Image(_imGuiTexture, new Num.Vector2(300, 150), Num.Vector2.Zero, Num.Vector2.One, Num.Vector4.One, Num.Vector4.One); // Here, the previously loaded texture is used
            }
        }


        public virtual void Layout(DebugInfo debugInfo, Dictionary<String, String> additionalInfo, ref bool show)
        {

            if (show)
            {
            ImGui.Begin("Debug", ref show);
            ImGui.Text(string.Format("fps: {0:F3}", debugInfo.fps));
            ImGui.Text(string.Format("Draw Time: {0:F3}", debugInfo.drawTime.TotalMilliseconds.ToString()));
            ImGui.Text(string.Format("Update Time: {0:F3}", debugInfo.updateTime.TotalMilliseconds.ToString()));
            ImGui.Text(string.Format("Tiles Drawn: {0:F}", debugInfo.tilesDrawn));


                ImGui.Text(string.Format("\nCamera Position: {0}", debugInfo.cameraPosition.ToString()));
            ImGui.Text(string.Format("\nGrid Position: {0} (has tree: {1})", debugInfo.mouseGrid.ToString(), debugInfo.hasTree));

            ImGui.Text(string.Format("Application average {0:F3} ms/frame ({1:F1} FPS", 1000f / ImGui.GetIO().Framerate, ImGui.GetIO().Framerate));



                if (ImGui.Button("Test Window"))
            {
                    this.show_test_window = !this.show_test_window;
                }
            if (debugInfo.pastFps.Length >= 1)
            {
                ImGui.PlotLines("Frame Rate", ref debugInfo.pastFps[0], debugInfo.pastFps.Length);
            }
                

                foreach (string k in additionalInfo.Keys)
                {
                    ImGui.Text(string.Format("{0}: {1}", k, additionalInfo[k]));
                }
                ImGui.End();
            }
            

            if (this.show_test_window)
            {
                ImGui.SetNextWindowPos(new Num.Vector2(650, 20), ImGuiCond.FirstUseEver);
                ImGui.ShowDemoWindow(ref show_test_window);
            }

        }
    }
}
