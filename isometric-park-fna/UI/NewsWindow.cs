
using ImGuiNET;

using Num = System.Numerics;
using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using YamlDotNet.Serialization;

namespace isometricparkfna.UI
{

        public struct NewsItem {
                public string hed; 
                public string contents; 
                public string source;
                public Dictionary<string, string> variables;

                public NewsItem Flatten(TraceryNet.Grammar grammar) {

                        var variableString = "#";

                        if (this.variables != null) {
                                foreach (var variable in this.variables) {
                                        variableString += String.Format("[{0}:{1}]", variable.Key, variable.Value);
                                }
                        }

                        variableString += "vars# ";

                        var result = grammar.Flatten(variableString);
#if DEBUG
                        System.Console.Write(variableString);
#endif

                        return new NewsItem {
                                hed = grammar.Flatten(this.hed),
                                    contents = grammar.Flatten(contents),
                                    source = grammar.Flatten(this.source)
                        };
                }



                public static List<NewsItem> FromYaml(string yamlString) {
                        var input = new StringReader(yamlString);

                        var deserializer = new DeserializerBuilder()
                                .Build();

                        //Dictionary<string, string>
                        var items = deserializer.Deserialize<List<NewsItem>>(input);

                        return items;
                }
        }

        public static class NewsWindow
    {

        public static void Render(ref bool show, ImFontPtr font, Simulation sim)
        {
                if (show)
                {
                        ImGui.PushFont(font);

                        ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.None;
                        ImGui.PushStyleVar(ImGuiStyleVar.FrameRounding, 0.0f);
                        ImGui.PushStyleVar(ImGuiStyleVar.WindowRounding, 0.0f);
                        ImGui.PushStyleVar(ImGuiStyleVar.FrameBorderSize, 1.0f);
                        ImGui.PushStyleVar(ImGuiStyleVar.TabRounding, 0.0f);
                        ImGui.PushStyleColor(ImGuiCol.WindowBg, new Num.Vector4(0.75f, 0.75f, 0.75f, 1f));
                        ImGui.PushStyleColor(ImGuiCol.Header, new Num.Vector4(0.75f, 0.75f, 0.75f, 1f));
                        ImGui.PushStyleColor(ImGuiCol.HeaderHovered, new Num.Vector4(0.75f, 0.75f, 0.75f, 1f));
                        ImGui.PushStyleColor(ImGuiCol.HeaderActive, new Num.Vector4(0.75f, 0.75f, 0.75f, 1f));
                        ImGui.PushStyleColor(ImGuiCol.ButtonHovered, new Num.Vector4(0.75f, 0.75f, 0.75f, 1f));

                        var title_bar = new Num.Vector4(0.65f, 0.65f, 0.65f, 1f);
                        ImGui.PushStyleColor(ImGuiCol.TitleBg, title_bar);
                        ImGui.PushStyleColor(ImGuiCol.TitleBgActive, title_bar);
                        ImGui.PushStyleColor(ImGuiCol.TitleBgCollapsed, title_bar);

                        ImGui.PushStyleColor(ImGuiCol.Border, new Num.Vector4(0f, 0f, 0f, 1f));
                        ImGui.PushStyleColor(ImGuiCol.BorderShadow, new Num.Vector4(0f, 0f, 0f, 0.5f));



                        ImGui.PushStyleColor(ImGuiCol.Button, new Num.Vector4(0.75f, 0.75f, 0.75f, 1f));
                        ImGui.PushStyleColor(ImGuiCol.Text, new Num.Vector4(0f, 0f, 0f, 1f));
			ImGui.SetNextWindowSize(new Num.Vector2(400, 400));
                        ImGui.Begin("NEWS", ref show, ImGuiWindowFlags.NoResize | ImGuiWindowFlags.NoCollapse | ImGuiWindowFlags.NoSavedSettings);

			var content = sim.latestNewsItems;
                        

                        if (ImGui.BeginTabBar("Sources", 0)) {
                                if (ImGui.BeginTabItem("Wire")) {
                                        foreach (NewsItem story in content.Where( s => (s.source == "Wire" ) ).Take(3)) {
                                                if (ImGui.TreeNode(story.hed)) {
                                                        ImGui.TextWrapped(story.contents);
                                                        ImGui.TreePop();
                                                }
                                        }
                                        ImGui.EndTabItem();
                                }
                                ImGui.EndTabItem();
                                if (ImGui.BeginTabItem("The Naturalist")) {
                                        foreach (NewsItem story in content.Where( s => (s.source == "Arborist" ) ).Take(3)) {
                                                if (ImGui.TreeNode(story.hed)) {
                                                        ImGui.TextWrapped(story.contents);
                                                        ImGui.TreePop();
                                                }
                                        }
                                        ImGui.EndTabItem();
                                }
                                if (ImGui.BeginTabItem("All True News")) {
                                        foreach (NewsItem story in content.Where( s => (s.source == "True" ) ).Take(3)) {
                                                if (ImGui.TreeNode(story.hed)) {
                                                        ImGui.TextWrapped(story.contents);
                                                        ImGui.TreePop();
                                                }
                                        }
                                        ImGui.EndTabItem();
                                }
                        }

                        ImGui.EndTabBar();


                        if (ImGui.Button("Okay"))
                        {
                                show = false;
                        }

                        ImGui.End();
                        ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.Left;
                        ImGui.PopStyleVar(4);
                        ImGui.PopStyleColor(12);
                        ImGui.PopFont();
                }
        }
    }
}
