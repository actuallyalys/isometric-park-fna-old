using ImGuiNET;

using Num = System.Numerics;

namespace isometricparkfna.UI
{

 public static class ForestWindow
    {
        public static void Render(ref bool show, ImFontPtr font, Simulation sim)
        {
			if (show)
			{
				ImGui.PushFont(font);

				ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.None;
				ImGui.PushStyleVar(ImGuiStyleVar.FrameRounding, 0.0f);
				ImGui.PushStyleVar(ImGuiStyleVar.WindowRounding, 0.0f);
				ImGui.PushStyleVar(ImGuiStyleVar.FrameBorderSize, 1.0f);
				ImGui.PushStyleColor(ImGuiCol.WindowBg, new Num.Vector4(0.75f, 0.75f, 0.75f, 1f));

				var title_bar = new Num.Vector4(0.65f, 0.65f, 0.65f, 1f);
				ImGui.PushStyleColor(ImGuiCol.TitleBg, title_bar);
				ImGui.PushStyleColor(ImGuiCol.TitleBgActive, title_bar);
				ImGui.PushStyleColor(ImGuiCol.TitleBgCollapsed, title_bar);

				ImGui.PushStyleColor(ImGuiCol.Border, new Num.Vector4(0f, 0f, 0f, 1f));
				ImGui.PushStyleColor(ImGuiCol.BorderShadow, new Num.Vector4(0f, 0f, 0f, 0.5f));



				ImGui.PushStyleColor(ImGuiCol.Button, new Num.Vector4(0.75f, 0.75f, 0.75f, 1f));
				ImGui.PushStyleColor(ImGuiCol.Text, new Num.Vector4(0f, 0f, 0f, 1f));
				ImGui.Begin("Forest Policy", ref show, ImGuiWindowFlags.NoResize | ImGuiWindowFlags.NoCollapse | ImGuiWindowFlags.AlwaysAutoResize | ImGuiWindowFlags.NoSavedSettings);


                int new_tree_planting = sim.tree_planting;
                ImGui.SliderInt("Tree Planting ", ref new_tree_planting, 0, Simulation.MAX_TREES_TO_PLANT, string.Format("%d (${0})", new_tree_planting*Simulation.TREE_PLANT_COST));
                sim.tree_planting = new_tree_planting;

                int new_tree_clearing = sim.tree_clearing;
                ImGui.SliderInt("Tree Clearing", ref new_tree_clearing, 0, Simulation.MAX_TREES_TO_CLEAR, string.Format("%d (${0})", new_tree_clearing*Simulation.TREE_CLEAR_COST));
                sim.tree_clearing = new_tree_clearing;

                ImGui.Text(string.Format("Crowded Trees: {0}", sim.crowded_trees ));
                ImGui.Text(string.Format("Percent Healthy Trees: {0:F2}", sim.healthy_percent));
                ImGui.Text(string.Format("Average Age of Trees: {0:F2}", sim.average_tree_age));
                ImGui.Text(string.Format("Max Age of Trees: {0:F2}", sim.max_tree_age));

                if (ImGui.Button("Okay"))
                {
                    show = false;
                }

				ImGui.End();
                ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.Left;
				ImGui.PopStyleVar(3);
				ImGui.PopStyleColor(8);
				ImGui.PopFont();
			}
		}
	}
}