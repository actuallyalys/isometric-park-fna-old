﻿using System;
using System.Collections.Generic;
using ImGuiNET;
using isometricparkfna.Utils;
using TraceryNet;
using Num = System.Numerics;


namespace isometricparkfna.UI
{


    //#nullable enable
    public struct DialogOption
    {
        public String? choice;
        public String response;
		public String speaker;
    }


    public static class DialogTrees
    {

        public static Node<DialogOption> introTree = new Node<DialogOption>(
            new DialogOption{response = "Welcome to your new park, director! You can use the mouse or arrow keys to move around, and the plus and minus keys to zoom in and out. B opens the budget and F lets you adjust Forest Policy.",
				speaker = "The Governor"
		                },
		    new Node<DialogOption>(new DialogOption { choice = "Okay",
                 response = "Make sure that you keep visitors happy and the budget in the black! You're currently getting an annual grant out of my budget—it'd sure be nice if you park were self-sufficient so we could drop that expense!",
				speaker = "The Governor"
			},
		    new Node<DialogOption>[]{
                new Node<DialogOption>(new DialogOption {choice="And I need to keep the forest healthy, too, right?", response="Uhh yeah.", speaker = "The Governor" },
                                        new Node<DialogOption>(new DialogOption {choice="...", speaker = "The Governor"})),
                new Node<DialogOption>(new DialogOption {choice="Sounds good!", response="I'll check in soon.", speaker = "The Governor" })

            })
		    );

		public static Node<DialogOption> testTree = new Node<DialogOption>(
			new DialogOption
			{
				response = "#[name:player]addressGreeting#",
				speaker = "#assistantName#"
			},
			
			new Node<DialogOption>[]{
				new Node<DialogOption>(new DialogOption {choice="Hi.", response="Bye!",
				speaker = "#assistantName#" }),
				new Node<DialogOption>(new DialogOption {choice="How are you?", response="#howdoing#",
				speaker = "#assistantName#" })

			}
			);

		public static Node<DialogOption> testTree2 = new Node<DialogOption>(
			new DialogOption
			{
				response = "#whatever#",
				speaker = "#assistantName#"
			},

			new Node<DialogOption>[]{
				new Node<DialogOption>(new DialogOption {choice="Hi.", response="Bye!",
				speaker = "#assistantName#" }),
				new Node<DialogOption>(new DialogOption {choice="How are you?", response="#howdoing#",
				speaker = "#assistantName#" })

			}
			);


		public static Node<DialogOption> flatten (Node<DialogOption> node, Grammar grammar) {

			DialogOption new_data = new DialogOption
			{
				choice = node.data.choice != null ? grammar.Flatten(node.data.choice) : node.data.choice,
				response = node.data.response != null ? grammar.Flatten(node.data.response) : node.data.response,
				speaker = node.data.speaker != null ? grammar.Flatten(node.data.speaker) : node.data.speaker
			};


			if (node.children != null) {
				List<Node<DialogOption>>  new_children = new List<Node<DialogOption>>();
				foreach (Node<DialogOption> child in node.children)
				{
					new_children.Add(flatten(child, grammar));
				}

				return new Node<DialogOption>(new_data, new_children.ToArray());
			}
			else
            {
				return new Node<DialogOption>(new_data);

			}

			
		}

	}

    public static class DialogInterface
    {
        public static Node<DialogOption> RenderDialog(ref bool show, ref bool paused, ImFontPtr font, Node<DialogOption> currentNode)
        {
			Node<DialogOption> new_child = currentNode;
			if (show)
			{
				ImGui.PushFont(font);

				ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.None;
				ImGui.PushStyleVar(ImGuiStyleVar.FrameRounding, 0.0f);
				ImGui.PushStyleVar(ImGuiStyleVar.WindowRounding, 0.0f);
				ImGui.PushStyleVar(ImGuiStyleVar.FrameBorderSize, 1.0f);
				ImGui.PushStyleColor(ImGuiCol.WindowBg, new Num.Vector4(0.75f, 0.75f, 0.75f, 1f));

				var title_bar = new Num.Vector4(0.65f, 0.65f, 0.65f, 1f);
				ImGui.PushStyleColor(ImGuiCol.TitleBg, title_bar);
				ImGui.PushStyleColor(ImGuiCol.TitleBgActive, title_bar);
				ImGui.PushStyleColor(ImGuiCol.TitleBgCollapsed, title_bar);

				ImGui.PushStyleColor(ImGuiCol.Border, new Num.Vector4(0f, 0f, 0f, 1f));
				ImGui.PushStyleColor(ImGuiCol.BorderShadow, new Num.Vector4(0f, 0f, 0f, 0.5f));



				ImGui.PushStyleColor(ImGuiCol.Button, new Num.Vector4(0.75f, 0.75f, 0.75f, 1f));
				ImGui.PushStyleColor(ImGuiCol.Text, new Num.Vector4(0f, 0f, 0f, 1f));
                                ImGui.SetNextWindowSize(new Num.Vector2(400, 200));
				ImGui.Begin(currentNode.data.speaker, ref show);


				if (currentNode.data.response != null)
                {
					string messageText = currentNode.data.response;
					ImGui.TextWrapped(messageText);
				}

				if ((currentNode.children != null) && currentNode.children.Length > 0)
				{
					foreach (Node<DialogOption> child in currentNode.children)
					{
						string buttonText = child.data.choice;
						if (ImGui.Button(buttonText))
						{
							new_child = child;
						}
					}
				}
				else
				{
					if (ImGui.Button("Okay"))
					{
						show = false;
						paused = false;
					}
				}

				if (currentNode.data.response == null)
				{
					show = false;
					paused = false;

				}
				ImGui.End();
                ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.Left;
				ImGui.PopStyleVar(3);
				ImGui.PopStyleColor(8);
				ImGui.PopFont();
			}
			return new_child;

		}
	}


}
