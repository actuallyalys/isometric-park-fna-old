using ImGuiNET;

using Num = System.Numerics;

namespace isometricparkfna.UI
{

    public static class Menu
    {

        private static bool activeButton(string label, bool active, Num.Vector4 activeColor) {

            if (active) {
                    ImGui.PushStyleColor(ImGuiCol.Button, activeColor);
            }

            var result = ImGui.Button(label);

            if (active) {
                    ImGui.PopStyleColor();
            }

            return result;
        }
        public static void Render(ImFontPtr font, int width, ref bool quit, ref bool paused, ref int rate, ref bool show_budget, ref bool show_news, ref bool show_forest, string header)
        {
            ImGui.PushFont(font);

            ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.None;
            ImGui.PushStyleVar(ImGuiStyleVar.FrameRounding, 0.0f);
            ImGui.PushStyleVar(ImGuiStyleVar.WindowRounding, 0.0f);
            ImGui.PushStyleVar(ImGuiStyleVar.FrameBorderSize, 1.0f);
            ImGui.PushStyleColor(ImGuiCol.WindowBg, new Num.Vector4(0.75f, 0.75f, 0.75f, 1f));
            ImGui.PushStyleColor(ImGuiCol.MenuBarBg, new Num.Vector4(0.75f, 0.75f, 0.75f, 1f));
            ImGui.PushStyleColor(ImGuiCol.PopupBg, new Num.Vector4(0.75f, 0.75f, 0.75f, 1f));

            var title_bar = new Num.Vector4(0.65f, 0.65f, 0.65f, 1f);
            ImGui.PushStyleColor(ImGuiCol.TitleBg, title_bar);
            ImGui.PushStyleColor(ImGuiCol.TitleBgActive, title_bar);
            ImGui.PushStyleColor(ImGuiCol.TitleBgCollapsed, title_bar);

            ImGui.PushStyleColor(ImGuiCol.Border, new Num.Vector4(0f, 0f, 0f, 1f));
            ImGui.PushStyleColor(ImGuiCol.BorderShadow, new Num.Vector4(0f, 0f, 0f, 0.5f));



            ImGui.PushStyleColor(ImGuiCol.Button, new Num.Vector4(0.75f, 0.75f, 0.75f, 1f));
            ImGui.PushStyleColor(ImGuiCol.Text, new Num.Vector4(0f, 0f, 0f, 1f));

            if (ImGui.BeginMainMenuBar())
            {

                ImGui.Text(header);

                ImGui.SetCursorPosX(width - 425);

                if (Menu.activeButton("$ Budget", show_budget, new Num.Vector4(0.060f, 0.590f, 0.980f, 1f)))
                {
                    show_budget = !show_budget;

                }
                if (Menu.activeButton("\ue124 Forest", show_forest, new Num.Vector4(0.060f, 0.590f, 0.980f, 1f)))
                {
                    show_forest = !show_forest;

                }
                if (Menu.activeButton("\ue0bf News", show_news, new Num.Vector4(0.060f, 0.590f, 0.980f, 1f)))
                {
                    show_news = !show_news;

                }

                ImGui.Text("|");

                if (Menu.activeButton("\ue0ac Pause", paused, new Num.Vector4(0.060f, 0.590f, 0.980f, 1f)))
                {
                    paused = !paused;
                }
                if (Menu.activeButton("1", (rate == 0), new Num.Vector4(0.060f, 0.590f, 0.980f, 1f)))
                {
                    paused = false;
                    rate = 0;
                }
                else if (Menu.activeButton("2", (rate == 1), new Num.Vector4(0.060f, 0.590f, 0.980f, 1f)))
                {
                    paused = false;
                    rate = 1;
                }
                else if (Menu.activeButton("3", (rate == 2), new Num.Vector4(0.060f, 0.590f, 0.980f, 1f)))
                {
                    paused = false;
                    rate = 2;
                }
                else if (Menu.activeButton("4", (rate == 3), new Num.Vector4(0.060f, 0.590f, 0.980f, 1f)))
                {
                    paused = false;
                    rate = 3;
                }
#if DEBUG
                else if (Menu.activeButton("5", (rate == 4), new Num.Vector4(0.060f, 0.590f, 0.980f, 1f)))
                {
                    paused = false;
                    rate = 4;
                }
#endif


                ImGui.EndMainMenuBar();

                // ImGui.End();
                ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.Left;
                ImGui.PopStyleVar(3);
                ImGui.PopStyleColor(9);
                ImGui.PopFont();
            }
        }
    }
}
