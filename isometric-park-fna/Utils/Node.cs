﻿using System;
namespace isometricparkfna.Utils
{
    public class Node<T>
    {
        public Node<T>[] children; //TODO RECONSIDER??

        public T data;


        public Node()
        {
        }

        public Node(T data)
        {
            this.data = data;
        }

        public Node(T data, Node<T>[] children)
        {
            this.data = data;
            this.children = children;
        }

        public Node(T data, Node<T> child)
        {
            this.data = data;
            this.children = new Node<T>[] { child };
        }
    }
}
