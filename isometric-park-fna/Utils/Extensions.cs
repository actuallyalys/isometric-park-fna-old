
using System;
using System.Collections.Generic;


namespace isometricparkfna
{
	public static class Extensions
	{
		public static List<T> Shuffle<T> (this List<T> source) {
			Random rnd = new Random();

			for(int i = (source.Count-1); i >= 0 ; i--) {
				var j = rnd.Next(0, (source.Count-1));
				var tmp = source[j];
				source[j] = source[i];
				source[i] = tmp;
			}


			return source;

		}
	}
}

