﻿using System;
using System.Collections.Generic;

namespace isometricparkfna
{

    //class MapRow
    //{
    //    public List<MapCell> Columns = new List<MapCell>();
    //}

    public class CellMap
    {
        public List<List<Cell>> cells;
        public int MapWidth = 50;
        public int MapHeight = 50;

        public int ZoneWidth = 10;
        public int ZoneHeight = 10;

        public int tree_count
        {
            get
            {
                int count = 0;
                foreach (Cell cell in this.tree_cells())
                {
                    count++;
                }
                return count;
            }
        }

        public int tree_capacity
        {
            get
            {
                return MapWidth * MapHeight;
            }
        }

        public int remaining_tree_capacity
        {
            get
            {
                return this.tree_capacity - this.tree_count;
            }
        }


        public CellMap()
        {
            //TileMap(MapWidth, MapHeight);
        }

        public CellMap(int width, int height)
        {
            this.MapWidth = width;
            this.MapHeight = height;

            this.cells = new List<List<Cell>>();

            for (int i = 0; i < height; i++)
            {
                List<Cell> newRow = new List<Cell>();
                for (int j = 0; j < width; j++)
                {
                    newRow.Add(new Cell());
                }

                this.cells.Add(newRow);
            }

        }

        public System.Collections.Generic.IEnumerable<Cell> tree_cells()
        {
            foreach (List<Cell> row in cells)
            {
                foreach (Cell cell in row)
                {
                    if (cell.hasTree)
                    {
                        yield return cell;
                    }
                }
            }
        }

        public System.Collections.Generic.IEnumerable<Cell> tree_cells(int zone)
        {
            foreach (List<Cell> row in cells)
            {
                foreach (Cell cell in row)
                {
                    if (cell.hasTree)
                    {
                        yield return cell;
                    }
                }
            }
        }

        public System.Collections.Generic.IEnumerable<Cell> iterate_cells()
        {
            foreach (List<Cell> row in cells)
            {
                foreach (Cell cell in row)
                {
                    yield return cell;
                }
            }
        }

        public Boolean inBounds(int x, int y)
        {
            return MathUtils.Between(x, 0, MapWidth - 1) && MathUtils.Between(y, 0, MapHeight - 1);
        }

        private System.Collections.Generic.IEnumerable<Cell> iterate_neighbors(int x, int y)
        {
            //iterates over neighbors (clockwise starting at noon/midnight)
            if (inBounds(x, y + 1))
            {
                yield return this.cells[x - 1][y];
            }
            if (inBounds(x + 1, y + 1))
            {
                yield return this.cells[x + 1][y + 1];
            }
            if (inBounds(x + 1, y))
            {
                yield return this.cells[x + 1][y];
            }
            if (inBounds(x + 1, y - 1))
            {
                yield return this.cells[x + 1][y - 1];
            }
            if (inBounds(x, y - 1))
            {
                yield return this.cells[x][y - 1];
            }
            if (inBounds(x - 1, y-1))
            {
                yield return this.cells[x - 1][y-1];
            }
            if (inBounds(x - 1, y))
            {
                yield return this.cells[x - 1][y];
            }
            if (inBounds(x - 1, y + 1))
            {
                yield return this.cells[x - 1][y+1];
            }
        }

        private int countNeighbors(int x, int y)
        {
            int count = 0;
            foreach (Cell neighbor in this.iterate_neighbors(x, y))
            {
                if (neighbor.hasTree) {
                    count++;
                }
            }

            return count;
        }

        public System.Collections.Generic.IEnumerable<Cell> iterate_cells_with_neighbors(int neighbors)
        {
            for (int i = 0; i < MapHeight; i++)
            {
                List<Cell> newRow = new List<Cell>();
                for (int j = 0; j < MapWidth; j++)
                {
                    if (this.countNeighbors(i, j) >= neighbors)
                    {
                        yield return cells[i][j];
                    }
                }
            }


        }

        public enum CellStatus{
            Clear,
            LivingTree,
            DeadTree
        }

        public class Cell
        {
            // public Boolean _hasTree = false;
            public CellStatus status {
                get; 
                private set;
            }

            public Boolean hasTree {
                get {
                    return this.status == CellStatus.LivingTree;
                }
            }

            public DateTime planted;

            public void addTree(DateTime datetime) {
                this.status = CellStatus.LivingTree;

                this.planted = datetime;
            }

            public void removeTree() {
                this.status = CellStatus.Clear;
            }

            public void markTreeDead() {
                this.status = CellStatus.DeadTree;
            }
        }
    }
}