﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace isometricparkfna
{
    public class Line
    {
        public static Texture2D PixelTexture;
        public Line()
        {

        }

        public static void drawLine(SpriteBatch batch, Vector2 start, Vector2 stop, Color color)
        {
            drawLine(batch, start, stop, color, 0, 1);
        }

        public static void drawLine(SpriteBatch batch, Vector2 start, Vector2 stop, Color color, float depth)
        {
            drawLine(batch, start, stop, color, depth, 1);

        }

        public static void drawLine(SpriteBatch batch, Vector2 start, Vector2 stop, Color color, float depth, int width)
        {
            Vector2 line = stop - start;
            float angle = (float)Math.Atan2((double)line.Y, (double) line.X);
            Rectangle rect = new Rectangle((int)start.X, (int)start.Y, (int)Math.Round(line.Length()), width);
            batch.Draw(Line.PixelTexture, rect, null, color, angle, new Vector2(0,0), SpriteEffects.None, depth);

        }

        public static void initialize(GraphicsDevice graphics)
        {
            SpriteBatch spriteBatch = new SpriteBatch(graphics);
            Line.PixelTexture = new Texture2D(graphics, 1, 1);

            Line.PixelTexture.SetData<Color> (new Color[] { Color.White});
        }

        public static Vector2 departurePoint(Vector2 start, Vector2 stop, float width, float height)
        {
            if (MathUtils.Between(stop.X, 0, width ) && MathUtils.Between(stop.Y, 0, height)) {
                return stop;
            }

            float slope = (start.Y - stop.Y) / (start.X - stop.X);

            float intercept = (start.Y - (slope * start.X));

            if (stop.X < 0) { 
                float newY = slope * 0 + intercept;
                return new Vector2(0, newY);
            }
            else if (stop.Y < 0)
            {
                float newX = intercept / slope;
                return new Vector2(newX, 0);

            }
            
            else if (stop.Y > height)
            {
                float newX = (intercept + height) / slope;
                return new Vector2(newX, height);
            }
            else if (stop.X > width)
            {
                float newY = slope * width + intercept;
                return new Vector2(width, newY);
            }

            return stop;//TODO


        }
    }





}
