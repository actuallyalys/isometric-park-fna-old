﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace isometricparkfna
{

    public struct Edge {
		public Vector2 Start;
		public Vector2 End;

		public static bool operator == (Edge left, Edge right) {
			return (left.Start == right.Start && left.End == right.End) ||
				(left.Start == right.End && left.End == right.Start);
		}

		public static bool operator != (Edge left, Edge right) {
			return !(left == right);
		}

		public override bool Equals(Object other) {
			return ((other != null) 
					&& ! this.GetType().Equals(other.GetType()) 
					&& this == (Edge)other); //we just confirmed the types match so this is okay
		}

		public override int GetHashCode() {
			return (Start, End).GetHashCode();
		}

    }
    public class Tile
    {
        //blatantly stolen from XNA resources.com
        static public int TileWidth = 64;
        static public int TileHeight = 64;
        // static public int TileStepX = 64;
        // static public int TileStepY = 16;
        //static public int OddRowXOffset = 32;

        static public int TileSpriteWidth = 64;
        static public int TileSpriteHeight = 32;

        static public Texture2D TileSetTexture;

        public Tile()
        {
        }

        static public Rectangle GetSourceRectangle(int tileIndex)
        {
            int tileY = tileIndex / (TileSetTexture.Width / TileWidth);
            int tileX = tileIndex % (TileSetTexture.Width / TileWidth);

            return new Rectangle(tileX * TileWidth, tileY * TileHeight, TileWidth, TileHeight);
        }

        static public Rectangle GetExtendedSourceRectangle(int tileIndex, int height)
        {
            int tileY = tileIndex / (TileSetTexture.Width / TileWidth);
            int tileX = tileIndex % (TileSetTexture.Width / TileWidth);

            return new Rectangle(tileX * TileWidth, tileY * TileHeight, TileWidth, TileHeight*height);
        }

        static public void OutlineSquare(SpriteBatch batch, float x, float y, Color color)
        {
            Tile.OutlineSquare(batch, x, y, color, 1);
        }

        static public void OutlineSquare(SpriteBatch batch, float x, float y, Color color, int size)
        {
            Vector2 adjust2 = new Vector2(Tile.TileSpriteWidth / 2, Tile.TileSpriteHeight); //TODO figure out why this second value shouldn't be halved

            //Upper right
            //float x = this.mouseGrid.X;
            //float y = this.mouseGrid.Y;

            Line.drawLine(batch,
                    new Vector2(((x - y) * Tile.TileSpriteWidth / 2), (x + y) * Tile.TileSpriteHeight / 2) + adjust2,
                                //new Vector2(this.squaresAcross * Tile.TileSpriteWidth, (y+1) * Tile.TileSpriteHeight),
                                new Vector2(((x - y + size) * Tile.TileSpriteWidth / 2), (x + y + size) * Tile.TileSpriteHeight / 2) + adjust2,
                    color, 0.79f);

            //Bottom right
            Line.drawLine(batch,
                    new Vector2(((x + size - y) * Tile.TileSpriteWidth / 2), (x + size + y) * Tile.TileSpriteHeight / 2) + adjust2,
                                //new Vector2(this.squaresAcross * Tile.TileSpriteWidth, (y+1) * Tile.TileSpriteHeight),
                                new Vector2(((x + size - (y + size)) * Tile.TileSpriteWidth / 2), (x + size + (y + size)) * Tile.TileSpriteHeight / 2) + adjust2,
                    color, 0.79f);
            //Bottom left
            Line.drawLine(batch,
                    new Vector2(((x - (y + size)) * Tile.TileSpriteWidth / 2), (x + y + size) * Tile.TileSpriteHeight / 2) + adjust2,
                                //new Vector2(this.squaresAcross * Tile.TileSpriteWidth, (y+1) * Tile.TileSpriteHeight),
                                new Vector2(((x + size - (y + size)) * Tile.TileSpriteWidth / 2), (x + size + (y + size)) * Tile.TileSpriteHeight / 2) + adjust2,
                    color, 0.79f);
            //Upper left
            Line.drawLine(batch,
                    new Vector2(((x - y) * Tile.TileSpriteWidth / 2), (x + y) * Tile.TileSpriteHeight / 2) + adjust2,
                                //new Vector2(this.squaresAcross * Tile.TileSpriteWidth, (y+1) * Tile.TileSpriteHeight),
                                new Vector2(((x - (y + size)) * Tile.TileSpriteWidth / 2), (x + (y + size)) * Tile.TileSpriteHeight / 2) + adjust2,
                    color, 0.79f);
        }


        static public void OutlineSquare2(SpriteBatch batch, float x, float y, Color color, int size)
        {
            Vector2 adjust2 = new Vector2(Tile.TileSpriteWidth / 2, Tile.TileSpriteHeight); //TODO figure out why this second value shouldn't be halved

            //Upper right
            //float x = this.mouseGrid.X;
            //float y = this.mouseGrid.Y;
	    //
	    int size_less_one = size - 1;

            Line.drawLine(batch,
                    new Vector2(((x - y) * Tile.TileSpriteWidth / 2), (x + y) * Tile.TileSpriteHeight / 2) + adjust2,
                                //new Vector2(this.squaresAcross * Tile.TileSpriteWidth, (y+1) * Tile.TileSpriteHeight),
                                new Vector2(((x - y + size_less_one) * Tile.TileSpriteWidth / 2), (x + y + size_less_one) * Tile.TileSpriteHeight / 2) + adjust2,
                    color, 0.79f);

            //Bottom right
            Line.drawLine(batch,
                    new Vector2(((x + size - y) * Tile.TileSpriteWidth / 2), (x + size_less_one + y) * Tile.TileSpriteHeight / 2) + adjust2,
                                //new Vector2(this.squaresAcross * Tile.TileSpriteWidth, (y+1) * Tile.TileSpriteHeight),
                                new Vector2(((x + size - (y + size)) * Tile.TileSpriteWidth / 2), (x + size_less_one + (y + size_less_one)) * Tile.TileSpriteHeight / 2) + adjust2,
                    color, 0.79f);
            //Bottom left
            Line.drawLine(batch,
                    new Vector2(((x - (y + size)) * Tile.TileSpriteWidth / 2), (x + y + size) * Tile.TileSpriteHeight / 2) + adjust2,
                                //new Vector2(this.squaresAcross * Tile.TileSpriteWidth, (y+1) * Tile.TileSpriteHeight),
                                new Vector2(((x + size - (y + size)) * Tile.TileSpriteWidth / 2), (x + size + (y + size)) * Tile.TileSpriteHeight / 2) + adjust2,
                    color, 0.79f);
            //Upper left
            Line.drawLine(batch,
                    new Vector2(((x - y) * Tile.TileSpriteWidth / 2), (x + y) * Tile.TileSpriteHeight / 2) + adjust2,
                                //new Vector2(this.squaresAcross * Tile.TileSpriteWidth, (y+1) * Tile.TileSpriteHeight),
                                new Vector2(((x - (y + size)) * Tile.TileSpriteWidth / 2), (x + (y + size)) * Tile.TileSpriteHeight / 2) + adjust2,
                    color, 0.79f);
        }

	public static void drawOutline(SpriteBatch batch, Color color) {
		Vector2[] squares = {new Vector2(15, 1), new Vector2(15,2)};

		foreach (Vector2 square in squares) {
		}
	}

	public static void drawEdge(SpriteBatch batch, Edge edge, Color color) {

	    //x = edge.Start.X
	    //y = edge.Start.y
	    //y + size = edge.End.y
	    //x + size = edge.End.x

            Vector2 adjust2 = new Vector2(Tile.TileSpriteWidth / 2, Tile.TileSpriteHeight); //TODO figure out why this second value shouldn't be halved

            Line.drawLine(batch,
                    new Vector2(((edge.Start.X - edge.Start.Y) * Tile.TileSpriteWidth / 2), (edge.Start.X + edge.Start.Y) * Tile.TileSpriteHeight / 2) + adjust2,
                                //new Vector2(this.squaresAcross * Tile.TileSpriteWidth, (y+1) * Tile.TileSpriteHeight),
                                new Vector2(((edge.End.X - edge.End.Y) * Tile.TileSpriteWidth / 2), (edge.End.X + edge.End.Y) * Tile.TileSpriteHeight / 2) + adjust2,
                    color, 0.79f);
	}

	public static void drawEdges(SpriteBatch batch, Edge[] edges, Color color) {
		foreach (Edge edge in edges) {
			drawEdge(batch, edge, color);
		}
	}

	public static void DrawOutlinedSquares(SpriteBatch batch, Vector2[] squares, Color color) {
		List<Edge> edges = new List<Edge>();

		foreach (Vector2 square in squares) {
			//upper right
			edges.Add(new Edge {Start=square, End=square + new Vector2(1, 0)});
			//lower right
			edges.Add(new Edge {Start=square + new Vector2(1, 0), End=square + new Vector2(1, 1)});
			//lower left
			edges.Add(new Edge {Start=square + new Vector2(0, 1), End=square + new Vector2(1, 1)});
			//upper left
			edges.Add(new Edge {Start=square, End=square + new Vector2(0, 1)});
		}

		edges = edges.GroupBy(x => x).Where(grp => grp.Count() == 1).Select(grp => grp.Key).ToList<Edge>();

		drawEdges(batch, edges.ToArray(), color);
	}


	public static void drawTileAt(SpriteBatch batch, int x, int y, int tileIndex, int height, float depth)
	{

		int height_adjust = 0;

		if (height > 1) //not sure why this is necessary :/
		{
			height_adjust = height;
		}

		int adjustedx = x - height_adjust;
		int adjustedy = y - height_adjust;

		int screenx = (adjustedx - adjustedy) * Tile.TileSpriteWidth / 2;
		int screeny = (adjustedx + adjustedy) * Tile.TileSpriteHeight / 2;

        // if (this.cull(x, y))
        // {
        batch.Draw(
                Tile.TileSetTexture,
                    new Rectangle(
                            screenx,
                            screeny,
                            Tile.TileWidth, Tile.TileHeight * height),
                Tile.GetExtendedSourceRectangle(tileIndex, height),
                Color.White,
                    0.0f,
                    Vector2.Zero,
                    SpriteEffects.None,
                    depth);
		// }

	}


    }
}
