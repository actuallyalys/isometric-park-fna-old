

CORE_SOLUTION = ./isometric-park-fna-core.sln
FRAMEWORK_SOLUTION = ./isometric-park-fna.sln


core-debug:
	dotnet build ${CORE_SOLUTION} -f netcoreapp3.1

core-release:
	dotnet build ${CORE_SOLUTION} -f netcoreapp3.1 -c Release

framework-release:
	msbuild -restore:True ${FRAMEWORK_SOLUTION} -p:Configuration=Release


clean-obj:
	rm -r isometric-park-fna/obj/

framework-debug: clean-obj
	msbuild -restore:True ${FRAMEWORK_SOLUTION} -p:Configuration=Debug


run-core-debug: 
	cd isometric-park-fna/bin/Debug/netcoreapp3.1; LD_LIBRARY_PATH="../../../fnalibs/lib64" DYLD_LIBRARY_PATH="/Users/alys/repos/isometric-park-fna/fnalibs/osx" dotnet ./isometric-park-fna.dll

run-core-release: 
	cd isometric-park-fna/bin/Release/netcoreapp3.1; LD_LIBRARY_PATH="../../../fnalibs/lib64" DYLD_LIBRARY_PATH="/Users/alys/repos/isometric-park-fna/fnalibs/osx" dotnet ./isometric-park-fna.dll

# lint:
# 	yamllint -d relaxed  isometric-park-fna/Content/news_items.yaml

#CONVENIENCE

# Just using framework for releases since I know it works for now:
release: framework-release

run: framework-debug
	cd isometric-park-fna/bin/Debug/; LD_LIBRARY_PATH="../../../fnalibs/lib64" DYLD_LIBRARY_PATH="/Users/alys/repos/isometric-park-fna/fnalibs/osx" mono isometric-park-fna.exe
	



